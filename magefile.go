//+build mage

package main

import (
	"runtime"

	"github.com/magefile/mage/mg"
	"github.com/magefile/mage/target"

	"gitlab.com/paars/bakyun/magician"
)

var (
	isWin = runtime.GOOS == "windows"

	Default = Build
)

func NPM() error {
	magician.Headline("NPM")

	build, err := target.Dir("assets", "src")
	if err != nil {
		return err
	}

	if !build {
		magician.Echo("skipping, already up to date!")
		return nil
	}

	return magician.Exec("npm", "start")
}

func Generate() error {
	magician.Headline("Generate")
	build, err := target.Dir("assets_generated.go", "assets", "templates")
	if err != nil {
		return err
	}

	if !build {
		magician.Echo("skipping, already up to date!")
		return nil
	}
	return magician.Exec("go", "generate", "./...")
}

func Build() {
	mg.SerialDeps(NPM, Generate)
}
