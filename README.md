# bakyun/frontend

This repository stores all materials for the frontend of bakyun.

## Install

For installation instructions, please look into the [bakyun/backend repository](https://gitlab.com/paars/bakyun/backend).

## Contribute

- Fork this repository
- Create a new feature branch for a new functionality or bugfix
- Commit your changes
- Push your code and open a new pull request
- Use [issues](https://gitlab.com/paars/bakyun/frontend/issues) for any questions
- Check [wiki](https://gitlab.com/paars/bakyun/frontend/wikis/home) for extra documentation

## Attributions

The design has been gracefully donated by [Enra](https://github.com/sr229).
If you like their work, please support them on [PayPal](https://paypal.me/chinodesuuu) or [Patreon](https://patreon.com/capuccino)
